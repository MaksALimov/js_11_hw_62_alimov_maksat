import React from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick';
import './SolarSystemPlanets.css';

const photos = [
    {
        name: 'Луна',
        url: 'https://solarsystem.nasa.gov/system/feature_items/mp4_videos/508_Moon_12sec.mp4',
        description: 'Луна — единственный естественный спутник Земли.',
        info: '/Moon',
    },
    {
        name: 'Сатурн',
        url: 'https://solarsystem.nasa.gov/system/feature_items/webm_videos/515_MED_saturn_time_lapse_0001.webm',
        description: 'Сатурн — шестая планета от Солнца и вторая по размерам планета в Солнечной системе.',
        info: '/Saturn',
    },
    {
        name: 'Юпитер',
        url: 'https://solarsystem.nasa.gov/system/feature_items/webm_videos/38_jupiter.webm',
        description: 'Юпитер — крупнейшая планета Солнечной системы, пятая по удалённости от Солнца.',
        info: '/Jupiter',
    },
    {
        name: 'Марс',
        url: 'https://solarsystem.nasa.gov/system/feature_items/webm_videos/37_Mars.webm',
        description: 'Марс — четвёртая по удалённости от Солнца и седьмая по размеру планета Солнечной системы',
        info: '/Mars',
    },
];

const SolarSystemPlanets = ({history}) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return (
        <div className="max">
            <Slider {...settings}>
                {photos.map((photo, i) => {
                    return (
                        <div key={i} >
                            <video loop autoPlay muted>
                                <source src={photo.url} type="video/webm"/>
                            </video>
                            <div className="planet-wrapper">
                                <p className="planet-wrapper__name">{photo.name}</p>
                                <p className="planet-name__small-descr">
                                    {photo.description}
                                </p>
                                <button onClick={() => history.push(photo.info)} className="planet-wrapper__info">Получить больше информации</button>
                            </div>
                        </div>
                    )
                })}
            </Slider>
        </div>
    );

};

export default SolarSystemPlanets;