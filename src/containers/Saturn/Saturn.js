import React from 'react';
import './Saturn.css';

const photos = [
    {url: 'https://in-space.ru/wp-content/uploads/2018/12/saturn_2.jpg'},
    {url: 'https://in-space.ru/wp-content/uploads/2018/12/saturn_4.jpg'},
    {url: 'https://in-space.ru/wp-content/uploads/2018/12/saturn_8.jpg'},
    {url: 'https://images11.popmeh.ru/upload/img_cache/71a/71aad2cdab2e97ec83fec19ec73915fa_ce_918x490x30x0_cropped_666x444.webp'},
    {url: 'https://images11.popmeh.ru/upload/img_cache/91a/91abce7ced225a181f74c873990d3cb2_cropped_666x666.webp'},
    {url: 'https://images11.popmeh.ru/upload/img_cache/782/782e2f26276bf2daf6fa58a106952c9b_cropped_480x480.webp'},

]

const Saturn = () => {
    return (
          <div className="SaturnWrapper">
              <img src="https://in-space.ru/wp-content/uploads/2018/12/earth_saturn.jpg" alt="Saturn"/>
              <p className="size-descr">Размеры Сатурна и Земли</p>
              <div className="research">
                  <h3 className="research-title">Исследование сатурна</h3>
                  <ol className="research-list">
                      <li>
                          Впервые наблюдая Сатурн в телескоп в 1609 – 1610 годах, Галилео Галилей заметил, что планета
                          выглядит как три тела, почти касающиеся друг друга, и предположил, что это два крупных
                          «компаньона» Сатурна, однако 2 года спустя не нашел тому подтверждение.
                      </li>
                      <li>
                          В 1659 году Христиан Гюйгенс с помощью более мощного телескопа выяснил, что «компаньоны» – это
                          на самом деле тонкое плоское кольцо, опоясывающее планету и не касающееся ее.
                      </li>
                      <li>
                          В 1979 году автоматическая межпланетная станция «Pioneer 11» впервые в истории пролетела вблизи
                          Сатурна, получив изображения планеты и некоторых ее спутников и открыв кольцо F.
                      </li>
                      <li>
                          В 1980 – 1981 годах систему Сатурна также посетили «Voyager-1» и «Voyager-2». Во время сближения
                          с планетой был сделан ряд фотографий в высоком разрешении и получены данные о температуре и
                          плотности атмосферы Сатурна, а также физических характеристиках его спутников, в том числе
                          Титана.
                      </li>
                      <li>
                          С 1990-х Сатурн, его спутники и кольца неоднократно исследовались космическим телескопом
                          «Hubble».
                      </li>
                      <li>
                          В 1997 году к Сатурну была запущена миссия «Cassini-Huygens», которая после 7 лет полета 1 июля
                          2004 года достигла системы Сатурна и вышла на орбиту вокруг планеты. Зонд «Huygens» отделился от
                          аппарата и на парашюте 14 января 2005 года спустился на поверхность Титана, отобрав пробы
                          атмосферы. За 13 лет научной деятельности космический аппарат «Cassini» перевернул представление
                          ученых о системе газового гиганта. Миссия «Cassini» завершена 15 сентября 2017 года путем
                          погружения космического аппарата в атмосферу Сатурна.
                      </li>
                  </ol>
              </div>
              <div className="photos">
                  <h4 className="photos-title">Фотографии Сатурна</h4>
                  {photos.map((photo, i) => {
                      return <img key={i} src={photo.url} alt="Photos"/>
                  })}
              </div>
          </div>
    );
};

export default Saturn;