import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter, NavLink} from "react-router-dom";
import SolarSystemPlanets from "./containers/SolarSytemPlanets/SolarSystemPlanets";
import Moon from "./containers/Moon/Moon";
import Saturn from "./containers/Saturn/Saturn";
import Jupiter from "./containers/Jupiter/Jupiter";
import Mars from "./containers/Mars/Mars";

const App = () => (
    <BrowserRouter>
            <nav>
                <ul className="primary">
                    <li>
                        <NavLink to="/Moon">Луна</NavLink>
                    </li>
                    <li>
                        <NavLink to="/Saturn">Сатурн</NavLink>
                    </li>
                    <li>
                        <NavLink to="/Jupiter">Юпитер</NavLink>
                    </li>
                    <li>
                        <NavLink to="/Mars">Марс</NavLink>
                    </li>
                </ul>
            </nav>
        <Switch>
            <Route path="/" exact component={SolarSystemPlanets}/>
            <Route path="/Moon" component={Moon}/>
            <Route path="/Saturn" component={Saturn}/>
            <Route path="/Jupiter" component={Jupiter}/>
            <Route path="/Mars" component={Mars}/>
        </Switch>
    </BrowserRouter>
);

export default App;